const path = require('path');
const fsPromises = require('fs/promises');
const fs = require('fs');

const { constants } = fs;

/**
 * Build a path to a storage file
 * @param {string} filename
 * @returns {string}
 */
function getFilePath(filename) {
  return path.join(__dirname, 'files', filename);
}

async function add({ filepath, newFilename }) {
  return await fsPromises.copyFile(filepath, getFilePath(newFilename), constants.COPYFILE_EXCL);
}

/**
 * Opens the file as a readable stream
 * @param {File} file
 * @param {string} file.newFilename
 * @returns {Promise<ReadStream>}
 */
function open({ newFilename }) {
  return new Promise((resolve, reject) => {
    const readStream = fs.createReadStream(getFilePath(newFilename))
      .on('open', () => resolve(readStream))
      .on('error', reject);
  });
}

module.exports = {
  add,
  open,
};
