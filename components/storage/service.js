const storageRepository = require('./repository');

async function add(file) {
  return await storageRepository.add(file);
}

async function open(file) {
  return await storageRepository.open(file);
}

module.exports = {
  add,
  open,
};
