const filesService = require('./service');
const { NotFound, Forbidden } = require('../../utils/errors');

async function isFileExists(id) {
  const file = await filesService.findOne({ id });

  if (!file) {
    throw new NotFound('File not found');
  }

  return file;
}

function isUserFile(file, userId) {
  if (file.userId !== userId) {
    throw new Forbidden('You have no access to this file');
  }

  return file;
}

module.exports = {
  isFileExists,
  isUserFile,
};
