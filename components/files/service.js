const storageService = require('../storage/service');
const filesRepository = require('./repository');
const array = require('../../utils/array');

async function findOne(params) {
  return await filesRepository.findOne(params);
}

async function findAll(params) {
  return await filesRepository.findAll(params);
}

/**
 * @param {File | File[]} files
 * @param {string} userId
 * @returns {Promise<*>}
 */
async function saveFiles(files, userId) {
  const filesArr = array.toArray(files);
  await Promise.all(filesArr.map(storageService.add));
  const userFiles = array.addProperty(filesArr, 'userId', userId);
  return await filesRepository.createMany(userFiles);
}

async function pipeToResponse(file, response) {
  const readStream = await storageService.open(file);
  response.setHeader('Content-Type', file.mimetype);
  readStream.pipe(response);
}

module.exports = {
  findOne,
  findAll,
  saveFiles,
  pipeToResponse,
};
