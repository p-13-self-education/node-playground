const { sequelize } = require('../../models/index.js');
const { buildFilters } = require('../../utils/sequelize');

async function findOne(params) {
  return await sequelize.models.File.findOne({ where: buildFilters(params) });
}

async function findAll(params) {
  return await sequelize.models.File.findAll({ where: buildFilters(params) });
}

async function createMany(files) {
  return await sequelize.models.File.bulkCreate(files, { validate: true, returning: true });
}

module.exports = {
  findOne,
  findAll,
  createMany,
};
