const usersService = require('./service');
const { NotFound } = require('../../utils/errors');

async function isUserExists(id) {
  const user = await usersService.findOne({ id });

  if (!user) {
    throw new NotFound(`No user found with id ${id}`);
  }

  return user;
}

module.exports = {
  isUserExists,
};
