const usersRepository = require('./repository');

async function findAll(params) {
  return await usersRepository.findAll(params);
}

async function findOne(params) {
  return await usersRepository.findOne(params);
}

module.exports = {
  findAll,
  findOne,
};
