const { sequelize } = require('../../models/index.js');
const { buildFilters } = require('../../utils/sequelize');

async function findAll(params) {
  return await sequelize.models.User.findAll({ where: buildFilters(params) });
}

async function findOne(params) {
  return await sequelize.models.User.findOne({ where: buildFilters(params) });
}

module.exports = {
  findAll,
  findOne,
};
