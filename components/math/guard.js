function isPositiveInteger(number) {
  if (number < 1 || !Number.isSafeInteger(number)) {
    const err = new Error('Invalid number');
    err.statusCode = 400;
    throw err;
  }

  return number;
}

module.exports = {
  isPositiveInteger,
};
