const path = require('path');

const { createWorker } = require('../../utils/workers');

const WORKERS = {
  factorial: path.join(__dirname, 'workers', 'factorial.js'),
};

/**
 * Run in parallel getting factorial of provided number
 * @param {number | bigint} integer
 * @returns {Promise<string>}
 */
async function getFactorialAsync(integer) {
  return await createWorker(WORKERS.factorial, integer);
}

module.exports = {
  getFactorialAsync,
};
