const { parentPort, workerData } = require('worker_threads');

let result = 1n;

for (let i = 2n; i <= workerData; i += 1n) {
  result *= i;
}

parentPort.postMessage(result.toString());
