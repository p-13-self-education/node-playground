const { getFactorialAsync } = require('./service');

describe('Math service tests', () => {
  it('getFactorialAsync: factorial of number', async () => {
    const expected = '120';
    const actual = await getFactorialAsync(5);
    expect(actual).toEqual(expected);
  });

  it('getFactorialAsync: factorial of bigint', async () => {
    const expected = '120';
    const actual = await getFactorialAsync(5n);
    expect(actual).toEqual(expected);
  });
});
