const { isPositiveInteger } = require('./guard');

describe('Math guard tests', () => {
  it("isPositiveInteger: return the number if it's a positive safe integer", () => {
    expect(isPositiveInteger(1)).toEqual(1);
    expect(isPositiveInteger(12345)).toEqual(12345);
    expect(isPositiveInteger(Number.MAX_SAFE_INTEGER)).toEqual(Number.MAX_SAFE_INTEGER);
  });

  it('isPositiveInteger: throw an error if the number is not a positive safe integer', () => {
    const err = new Error('Invalid number');
    expect(() => isPositiveInteger(0)).toThrowError(err);
    expect(() => isPositiveInteger(-4521)).toThrowError(err);
    expect(() => isPositiveInteger(4.5)).toThrowError(err);
    expect(() => isPositiveInteger(Number.MAX_SAFE_INTEGER + 1)).toThrowError(err);
  });
});
