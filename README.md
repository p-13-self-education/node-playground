# Node playground
*The purpose of this project is just to learn node.js and have fun.*

## Configuration

Create the file ".env":

```dotenv
APP_HOST=0.0.0.0
APP_PORT=3000
PG_USER=vairel
PG_PASSWORD=vairel
PG_DB=development
NODE_ENV=development
```

Create the file "config/config.js":

```js
require('dotenv').config();

const env = process.env.NODE_ENV || 'development';
const host = process.env.DB_URL || 'localhost';

module.exports = {
  [env]: {
    username: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
    database: process.env.PG_DB,
    host,
    port: 5432,
    dialect: 'postgres',
  },
};
```
