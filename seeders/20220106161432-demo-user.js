module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert('Users', [
    {
      firstName: 'John',
      lastName: 'Doe',
      age: 32,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      firstName: 'Michael',
      lastName: 'Jordan',
      age: 41,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      firstName: 'Jennifer',
      lastName: 'Lopez',
      age: 50,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Users', null, {}),
};
