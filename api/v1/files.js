const filesService = require('../../components/files/service');
const filesGuard = require('../../components/files/guard');
const usersGuard = require('../../components/users/guard');
const { parseFormData } = require('../../utils/formidable');
const { BadRequest } = require('../../utils/errors');

/**
 * Fetch user id from request
 * @param {Request} request
 * @returns {number}
 */
function getUserId(request) {
  const { user_id: userId } = request.headers;

  if (!userId) {
    throw new BadRequest('Missing user_id header');
  }

  return Number(userId);
}

async function preHandler(request) {
  await usersGuard.isUserExists(getUserId(request));
}

module.exports = function initFilesApi(app) {
  app.addContentTypeParser('multipart/form-data', (request, payload, done) => done());

  app.get('/files', { preHandler }, async (request, reply) => {
    const files = await filesService.findAll({ userId: getUserId(request) });

    reply.send(files);
  });

  app.get('/files/:id/download', { preHandler }, async (request, reply) => {
    const { id } = request.params;

    const file = filesGuard.isUserFile(await filesGuard.isFileExists(id), getUserId(request));

    await filesService.pipeToResponse(file, reply.raw);
  });

  app.post('/files', { preHandler }, async (request, reply) => {
    const { files: { items } } = await parseFormData(request.raw);

    const files = await filesService.saveFiles(items, getUserId(request));

    reply.send(files);
  });
};
