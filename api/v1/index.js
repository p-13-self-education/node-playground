const initUsersApi = require('./users');
const initFilesApi = require('./files');
const initMathApi = require('./math');

module.exports = function initApiV1(app) {
  app.get('/', () => ({ hello: 'world' }));

  initUsersApi(app);
  initFilesApi(app);
  initMathApi(app);
};
