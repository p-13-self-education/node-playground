const userService = require('../../components/users/service');

module.exports = function initUsersApi(app) {
  app.get('/users', async (request, reply) => {
    const users = await userService.findAll(request.query);

    reply.send(users);
  });
};
