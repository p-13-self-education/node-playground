const mathService = require('../../components/math/service');
const mathGuard = require('../../components/math/guard');

module.exports = function initMathApi(app) {
  app.get('/math/factorial/:num', async ({ params }) => {
    const number = mathGuard.isPositiveInteger(Number(params.num));

    const result = await mathService.getFactorialAsync(number);

    return { result };
  });
};
