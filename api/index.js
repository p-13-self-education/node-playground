const fastify = require('fastify');

const initApiV1 = require('./v1');
const { ClientError } = require('../utils/errors');

const app = fastify({ logger: true });

app.setErrorHandler((error, req, reply) => {
  if (error instanceof ClientError) {
    app.log.info(error);
  } else {
    app.log.error(error);
  }
  reply.status(error.statusCode || 500).send(error);
});

initApiV1(app);

module.exports = app;
