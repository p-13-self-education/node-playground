FROM node:16

WORKDIR /node-app

COPY package.json package-lock.json ./

RUN npm ci

COPY . .

CMD ["npm", "run", "init"]
