const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class File extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ User }) {
      File.belongsTo(User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });
    }
  }
  File.init({
    userId: DataTypes.NUMBER,
    newFilename: DataTypes.STRING,
    originalFilename: DataTypes.STRING,
    mimetype: DataTypes.STRING,
    size: DataTypes.NUMBER,
  }, {
    sequelize,
    modelName: 'File',
  });
  return File;
};
