require('dotenv').config();
const { sequelize } = require('./models/index.js');
const app = require('./api/index.js');

const port = process.env.APP_PORT || 3000;
const host = process.env.APP_HOST || 'localhost';

(async function start() {
  try {
    await sequelize.authenticate();
    await app.listen(port, host);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}());
