/**
 * Converts incoming object into array
 * @param {any} obj
 * @returns {Array<any>}
 */
function toArray(obj) {
  return Array.isArray(obj) ? obj : [obj];
}

/**
 * Extends each array item with property
 * @param {Array<object>} arr
 * @param {string} name
 * @param {any} value
 * @returns {*}
 */
function addProperty(arr, name, value) {
  return arr.map((item) => ({ ...item, [name]: value }));
}

module.exports = {
  addProperty,
  toArray,
};
