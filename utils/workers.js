const { Worker } = require('worker_threads');

/**
 * Create worker in a separate thread
 * @param location {string}
 * @param workerData {any}
 * @returns {Promise<string>}
 */
function createWorker(location, workerData) {
  return new Promise((resolve, reject) => {
    const worker = new Worker(location, { workerData });
    worker.on('message', resolve);
    worker.on('error', reject);
    worker.on('exit', (code) => {
      if (code !== 0) reject(new Error(`Worker stopped with exit code ${code}`));
    });
  });
}

module.exports = {
  createWorker,
};
