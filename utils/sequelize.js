/* eslint-disable no-param-reassign */
const { Op } = require('sequelize');

const CHARS = {
  percent: '%',
};

/**
 * Convert filters object to sequelize filters
 * @param params {Record<string, string | string[]>}
 * @returns {Record<string, any>}
 */
function buildFilters(params) {
  return Object.keys(params).reduce((where, key) => {
    if (Array.isArray(params[key])) {
      where[key] = params[key];
      return where;
    }

    if (Number.isInteger(Number(params[key]))) {
      where[key] = Number(params[key]);
      return where;
    }

    if (params[key].includes(CHARS.percent)) {
      where[key] = {
        [Op.like]: params[key],
      };
      return where;
    }

    where[key] = params[key];

    return where;
  }, {});
}

module.exports = {
  buildFilters,
};
