/* eslint-disable max-classes-per-file */
class ClientError extends Error {}

class BadRequest extends ClientError {
  constructor(message) {
    super(message);
    this.statusCode = 400;
  }
}

class Unauthorized extends ClientError {
  constructor(message) {
    super(message);
    this.statusCode = 401;
  }
}

class Forbidden extends ClientError {
  constructor(message) {
    super(message);
    this.statusCode = 403;
  }
}

class NotFound extends ClientError {
  constructor(message) {
    super(message);
    this.statusCode = 404;
  }
}

module.exports = {
  ClientError,
  BadRequest,
  Unauthorized,
  Forbidden,
  NotFound,
};
