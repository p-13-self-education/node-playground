const formidable = require('formidable');

const form = new formidable.IncomingForm({
  multiples: true,
});

/**
 * @typedef {Object} File
 * @property {number} size
 * @property {string} filepath
 * @property {string | null} originalFilename
 * @property {string | null} newFilename
 * @property {string | null} mimetype
 * @property {Date | null} mtime
 * @property {false | 'sha1' | 'md5' | 'sha256'} hashAlgorithm
 * @property {string | object | null} hash
 */

/**
 * @typedef {Object} FormData
 * @property {Record<string, string | string[]>} fields
 * @property {Record<string, File | File[]>} files
 */

/**
 * Parse request form data asynchronously
 * @param req Node.js request object
 * @returns {Promise<FormData>}
 */
function parseFormData(req) {
  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) return reject(err);
      return resolve({ fields, files });
    });
  });
}

module.exports = {
  parseFormData,
};
